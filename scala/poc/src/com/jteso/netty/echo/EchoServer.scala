package com.jteso.netty.echo

import java.net.InetSocketAddress
import java.util.concurrent.Executors
import org.jboss.netty.bootstrap.ServerBootstrap
import org.jboss.netty.channel.ChannelPipeline
import org.jboss.netty.channel.ChannelPipelineFactory
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory
import org.jboss.netty.channel.Channels

object EchoServer {
	
  def main(args: Array[String]) {
    // Configure the Server
    val bootstrap = new ServerBootstrap(
        new NioServerSocketChannelFactory(Executors.newCachedThreadPool, Executors.newCachedThreadPool))
    
    // Set up the pipeline factory
    bootstrap.setPipelineFactory(new ChannelPipelineFactory {
      override def getPipeline: ChannelPipeline = Channels.pipeline(new EchoServerHandler)
    })
    
    // Bind and start to accept incoming connections
    bootstrap.bind(new InetSocketAddress(8080))
  }
}