package com.jteso.netty.discard

import org.jboss.netty.channel.ExceptionEvent
import org.jboss.netty.channel.ChannelStateEvent
import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.channel.SimpleChannelUpstreamHandler
import org.jboss.netty.channel.MessageEvent
import org.jboss.netty.channel.WriteCompletionEvent
import org.jboss.netty.channel.ChannelHandlerContext
import org.jboss.netty.channel.ChannelEvent
import org.jboss.netty.buffer.ChannelBuffers
import java.util.logging.Logger
import org.jboss.netty.channel.ChannelState

class DiscardClientHandler(messageSize: Int) extends SimpleChannelUpstreamHandler {
  require(messageSize > 0)

  private val logger = Logger.getLogger(getClass.getName)
  val content = new Array[Byte](messageSize)
  private var transferredBytes = 0L

  def getTransferredBytes: Long = transferredBytes

  override def handleUpstream(ctx: ChannelHandlerContext, e: ChannelEvent) {
    e match {
      case c: ChannelStateEvent => if (c.getState != ChannelState.INTEREST_OPS) logger.info(e.toString)
      case _ =>
    }
    // Let SimpleChannelHandler call actual event handler methods below.
    super.handleUpstream(ctx, e)
  }

  // Send the initial messages.
  override def channelConnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) { generateTraffic(e) }

  // Keep sending messages whenever the current socket buffer has room.
  override def channelInterestChanged(ctx: ChannelHandlerContext, e: ChannelStateEvent) { generateTraffic(e) }

  override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
    // Server is supposed to send nothing.  Therefore, do nothing.
  }

  override def writeComplete(ctx: ChannelHandlerContext, e: WriteCompletionEvent) {
    transferredBytes += e.getWrittenAmount
  }

  override def exceptionCaught(context: ChannelHandlerContext, e: ExceptionEvent) {
    // Close the connection when an exception is raised.
    logger.warning("Unexpected exception from downstream." + e.getCause)
    e.getChannel.close()
  }

  private def generateTraffic(e: ChannelStateEvent) {
    // Keep generating traffic until the channel is unwritable.
    // A channel becomes unwritable when its internal buffer is full.
    // If you keep writing messages ignoring this property,
    // you will end up with an OutOfMemoryError.
	logger.info("[Client] Generating traffic...")
    val channel = e.getChannel
    var moreMessages = true
    while (channel.isWritable && moreMessages) {
        val m: ChannelBuffer = nextMessage()
        if (m == null) moreMessages = false else channel.write(m)
    }
  }

  private def nextMessage(): ChannelBuffer = ChannelBuffers.wrappedBuffer(content)

}