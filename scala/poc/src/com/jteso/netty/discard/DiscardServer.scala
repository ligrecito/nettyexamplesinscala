package com.jteso.netty.discard

import org.jboss.netty.bootstrap.ServerBootstrap
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory
import java.util.concurrent.Executors
import org.jboss.netty.channel.ChannelPipelineFactory
import org.jboss.netty.channel.ChannelPipeline
import org.jboss.netty.channel.Channels
import java.net.InetSocketAddress
import java.util.logging.Logger

object DiscardServer {
	val logger = Logger.getLogger(getClass.getName)
	
	def main(args: Array[String]) {
	  // Configure the server
	  val bootstrap = new ServerBootstrap(
	      new NioServerSocketChannelFactory(Executors.newCachedThreadPool, Executors.newCachedThreadPool))
	  
	  // Set up the pipeline factory
	  bootstrap.setPipelineFactory(new ChannelPipelineFactory {
	    override def getPipeline: ChannelPipeline = Channels.pipeline(new DiscardServerHandler)
	  })
	  
	  // Bind and start accept incoming connections
	  bootstrap.bind(new InetSocketAddress(8080))
	  logger.info("[Server] Connected and listening...")
	}
}