package com.jteso.netty.discard

import java.util.logging.Logger
import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.channel.ChannelEvent
import org.jboss.netty.channel.ChannelHandlerContext
import org.jboss.netty.channel.ChannelStateEvent
import org.jboss.netty.channel.MessageEvent
import org.jboss.netty.channel.SimpleChannelUpstreamHandler
import org.jboss.netty.channel.ExceptionEvent

class DiscardServerHandler extends SimpleChannelUpstreamHandler {
  val logger = Logger.getLogger(getClass.getName)
  var transferredBytes = 0L
  
  def getTransferredBytes: Long = transferredBytes
  
  override def handleUpstream(ctx: ChannelHandlerContext, e: ChannelEvent) {
    e match {
      case c: ChannelStateEvent => logger.info(e.toString())
      case _ => None
    }
    super.handleUpstream(ctx, e)
  }
  
  override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
    // Discard received data silently by doing nothing
    transferredBytes = transferredBytes + ((e.getMessage match {
      case c: ChannelBuffer => c
      case _				=> throw new ClassCastException
    }) readableBytes)
    logger.info("[Server] Transferred bytes:" + transferredBytes)
  }
  
  override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
    logger.warning("WTF! unexpected exception from downstream." + e.getCause)
    e.getChannel.close
  }

}