package com.jteso.netty.uptime

import java.util.concurrent.Executors
import org.jboss.netty.bootstrap.ClientBootstrap
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory
import org.jboss.netty.util.HashedWheelTimer
import org.jboss.netty.channel.ChannelPipelineFactory
import org.jboss.netty.handler.timeout.ReadTimeoutHandler
import org.jboss.netty.channel.ChannelPipeline
import java.net.InetSocketAddress
import org.jboss.netty.channel.Channels

object UptimeClient {
  // Host and port to connect to
  val HOST = "localhost"
  val PORT = 8080
  
  // Sleep 5 seconds before reconnect with server
  val RECONNECT_DELAY = 5
  
  // Reconnect when server has sent anything for 10 seconds
  val READ_TIMEOUT = 10
  
  // Initialize the timer that will schedule subsequent reconnections with server
  val timer = new HashedWheelTimer

  // Configure the client
  val bootstrap = new ClientBootstrap(
      new NioClientSocketChannelFactory(Executors.newCachedThreadPool(), Executors.newCachedThreadPool()))
  
  // Set up the pipeline factory
  bootstrap.setPipelineFactory(new ChannelPipelineFactory{
	  private val timeoutHandler = new ReadTimeoutHandler(timer, READ_TIMEOUT)
	  private val uptimeClientHandler	 = new UptimeClientHandler(bootstrap, timer)
	  override def getPipeline: ChannelPipeline = Channels.pipeline(timeoutHandler, uptimeClientHandler)
  })
      
  bootstrap.setOption("remoteAddress", new InetSocketAddress(HOST, PORT))
  
  // Initiate the first connection attempt, the rest is handled by the UptimeHandler
  bootstrap.connect
  
}