package com.jteso.netty.echo

import org.jboss.netty.channel.SimpleChannelUpstreamHandler
import java.util.logging.Logger
import java.util.concurrent.atomic.AtomicLong
import org.jboss.netty.buffer.ChannelBuffers
import org.jboss.netty.channel.ChannelHandlerContext
import org.jboss.netty.channel.Channel
import org.jboss.netty.channel.ChannelStateEvent
import org.jboss.netty.channel.MessageEvent
import org.jboss.netty.buffer.ChannelBuffer
import org.jboss.netty.channel.ExceptionEvent

class EchoClientHandler(firstMessageSize: Int) extends SimpleChannelUpstreamHandler {

	require(firstMessageSize >0)
	
	private val logger = Logger.getLogger(getClass.getName)
	private val transferredBytes = new AtomicLong
	private val firstMessage = ChannelBuffers.buffer(firstMessageSize)
	
	//constructor code
	val range = 0.until(firstMessage.capacity)
	for (i <- range) { firstMessage.writeByte(i.toByte) }
	
	// methods
	def getTransferredBytes: Long = transferredBytes.get
	
	// send the first mesage. Server will not send anything here, because firstMessages capacity is 0
	override def channelConnected(ctx: ChannelHandlerContext, e: ChannelStateEvent) {
	  e.getChannel.write(firstMessage)
	}
	
	override def messageReceived(ctx: ChannelHandlerContext, e: MessageEvent) {
	  // Send back the received message to the remote peer
	  transferredBytes.addAndGet((e.getMessage() match {
	    case c:ChannelBuffer => c
	    case _ 				 => throw new ClassCastException
	  }) readableBytes())
	}
	
	override def exceptionCaught(ctx: ChannelHandlerContext, e: ExceptionEvent) {
	  logger.warning("Unexpected exception from downstream." + e.getCause)
	  e.getChannel.close
	}
}